<?php

namespace App\Http\Middleware;
use GuzzleHttp\Client;
use Closure;

class CountryMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

//        session()->remove('country_code');
        if(!session()->has('country')) {
            $http = new Client();
            $ip = $this->getIPAddress();

            $ip = "31.11.55.255";
            $response = json_decode($http->get('http://ip-api.com/json/' . $ip)->getBody());
            if (isset($response->countryCode)) {
                setCountryData($response->countryCode);
            }

        }


        return $next($request);
    }

    private function getIPAddress()
    {
        //whether ip is from the share internet
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } //whether ip is from the proxy
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } //whether ip is from the remote address
        else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }



}
