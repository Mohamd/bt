<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Darryldecode\Cart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    public function store(Request $request) {
        $request->validate([
            "product_id"=>'required',
            "qty"=>"required|numeric|min:1"
        ]);

        $product = Product::where('id',$request->product_id)->first();

        \Cart::add([
            'id' => $product->id, // inique row ID
            'name' => $product->name,
            'price' => $product->price,
            'quantity' => $request->qty,
            'attributes' => array(),
            'associatedModel'=>$product
        ]);

        return response()->json(trans('site.added'));
    }
}
