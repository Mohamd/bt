<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($slug){

        $page = Page::where('slug',$slug)->first();
        if ($page) {
            return view('page')->with(['page'=>$page]);
        }

        abort(404);

    }
}
