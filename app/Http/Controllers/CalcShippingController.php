<?php

namespace App\Http\Controllers;

use Darryldecode\Cart\CartCondition;
use Illuminate\Http\Request;

class CalcShippingController extends Controller
{
    public function index(Request $request) {
        $request->validate([
           'values'=>'required',
           'shipping_method'=>'required',
        ]);

        $method = $request->shipping_method;
        return $method($request->values);
    }


}
