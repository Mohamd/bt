<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use App\Models\Order;
use App\Models\OrderProduct;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;



class OrderController extends Controller
{
    public function store(Request  $request) {
        $request->validate([
            'name'=>'required',
            'phone'=>'required|min:3|max:14',

        ]);

        $data = $request->all();
        if (session()->get('country')->code == "KW")  {
            $request->validate([
                "payment_method" => "required|in:CASH ON DELIVERY,Credit Card",
                "area" => "required",
                "block" => "required",
                "street" => "required",
                "house" => "required",
            ]);

            $data['address'] = json_encode($request->only('area','block','street','house'));
            $data['shipping_method'] = "Wagon";

        } else {
            $request->validate([
                "zone" => "required",
                "city" => "required",
                "postcode" => "",
                "address" => "required",
            ]);

            $data['shipping_method'] = "DHL";
            $data['payment_method'] = 'Credit Card';

        }
        // calc subtotal

        $total_products = 0;

        $items = [];

        // add items to order products
        foreach (\Cart::getContent() as $item) {
            $cart_item = new OrderProduct();
            $cart_item->product_id = $item->associatedModel->id;
            $cart_item->quantity = $item->quantity;
            $cart_item->total = $item->price;
            array_push($items,$cart_item);
            $total_products += $item->quantity * $item->price;
        }

        $data['phone'] = session()->get('country')->dial_code.$data['phone'];
        $data['sub_total'] = $total_products;

        foreach (\Cart::getConditions() as $cond) {

            if ($cond->getType() =='shipping') {
                $data['shipping'] = $cond->getValue();
            } elseif($cond->getType()=='Coupon') {

                $data['coupon'] = session()->get('coupon');
                $data['coupon_price'] = $cond->parsedRawValue;
            }
        }

        $data['total'] = \Cart::getTotal();

        $order = new Order();

        $order->fill($data);

        $order->save();

        $order->invoice_number = "INV-".$order->id.rand(1,50000);
        $order->save();


        $order->orderProducts()->saveMany($items);



        if ($order->payment_method == "CASH ON DELIVERY") {
            $order->order_status = "PROCESSING";
            $order->save();
            session()->put('order_id',$order->id);
            session()->remove('coupon');
            \Cart::clear();
            return redirect()->to(route('success'));
        } else {
            if (session()->get('country')->code !== 'KW') {
                $response = $this->credit($order,true);
            } else {
                $response = $this->credit($order);
            }

            if (!$response) {

                return \redirect()->back()->withInput(request()->input());
            }

            session()->put('payment_old_inputs',json_encode(request()->input()));

            return view('redirect')->with(['url'=>$response]);


        }


    }

    private function credit(Order $order,$shipping=false) {
//        dd($order);

        $phone = $this->getPhone($order->phone);

        $data = [
                  "NotificationOption" => "ALL",
                  "CustomerName" => $order->name,
                  "DisplayCurrencyIso" => "KWD",
                  "MobileCountryCode" => $phone[0],
                  "CustomerMobile" => $phone[1],
                  "CustomerEmail" => $order->email,
                  "InvoiceValue" => $order->total,
                  "CallBackUrl" => route('call_back'),
                  "ErrorUrl" => route('call_back'),
                  "Language" => "AR",
                  "CustomerReference" => $order->id,
                ];

        if ($shipping == true) {
           $data["ShippingMethod"]= "1";
            $data["ShippingConsignee"] = [
                "PersonName"=>$order->name,
                "Mobile"=>$phone[1],
                "EmailAddress"=>$order->email,
                "LineAddress"=>$order->address,
                "CityName"=>$order->city,
                "PostalCode"=>$order->postcode ?? '-',
                "CountryCode"=>$order->zone,
            ];

            $all_items = [];


            $coupon = $order->coupon_price;

            $total_qty = 0;

            foreach (\Cart::getContent() as $item) {

                $product_item = [];

                $product_item['ItemName'] = $item->associatedModel->translate('en')->name;
                $product_item['Description'] = $item->associatedModel->translate('en')->name;
                $product_item['Weight'] = $item->associatedModel->weight;
                $product_item['Width'] = $item->associatedModel->width;
                $product_item['Height'] = $item->associatedModel->height;
                $product_item['Depth'] = $item->associatedModel->depth;
                $product_item['Quantity'] = $item->quantity;

                $product_item['UnitPrice'] = $item->price;

                $total_qty += $item->quantity;
                array_push($all_items,$product_item);
            }


            $persent_for_every_product = $coupon / $total_qty;

            foreach ($all_items as $index => $item){

                $all_items[$index]['UnitPrice'] =  number_format($item['UnitPrice'] - $persent_for_every_product,2);

            }


            $data['InvoiceItems'] = $all_items;

            $data['InvoiceValue'] =  number_format($data['InvoiceValue'] - $order->shipping,2);
        }



        try {

            $response = Http::withHeaders([
                'Authorization'=> 'Bearer '.setting('my-fatoorah.secret_key'),
                'Content-Type'=> 'application/json',

            ])->post(setting('my-fatoorah.url').'/SendPayment',$data);

            $response = json_decode($response->body());



            if (isset($response->Data->InvoiceId) && isset($response->Data->InvoiceURL)) {
                $order->payment_invoice = $response->Data->InvoiceId;
                $order->save();
//                dd($response->Data->InvoiceURL);
               return $response->Data->InvoiceURL;

            } else {
                alert()->error(trans('site.payment_error'), trans('site.error'));
                return false;
            }

        } catch (\e $exception) {

        }

        alert()->error(trans('site.payment_error'), trans('site.error'));
        return false;

    }

    private function getPhone($inputString) {

        //remove any arabic digit
        $newNumbers = range(0, 9);

        $persianDecimal = array('&#1776;', '&#1777;', '&#1778;', '&#1779;', '&#1780;', '&#1781;', '&#1782;', '&#1783;', '&#1784;', '&#1785;'); // 1. Persian HTML decimal
        $arabicDecimal  = array('&#1632;', '&#1633;', '&#1634;', '&#1635;', '&#1636;', '&#1637;', '&#1638;', '&#1639;', '&#1640;', '&#1641;'); // 2. Arabic HTML decimal
        $arabic         = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'); // 3. Arabic Numeric
        $persian        = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'); // 4. Persian Numeric

        $string0 = str_replace($persianDecimal, $newNumbers, $inputString);
        $string1 = str_replace($arabicDecimal, $newNumbers, $string0);
        $string2 = str_replace($arabic, $newNumbers, $string1);
        $string3 = str_replace($persian, $newNumbers, $string2);

        //Keep Only digits
        $string4 = preg_replace('/[^0-9]/', '', $string3);

        //remove 00 at start
        if (strpos($string4, '00') === 0) {
            $string4 = substr($string4, 2);
        }

        //$this->log->info($string4);
        //check for the allowed length
        $len = strlen($string4);
        if ($len < 3 || $len > 14) {
            throw new \Exception('Phone Number lenght must be between 3 to 14 digits');
        }

        //get the phone arr
        if (strlen(substr($string4, 3)) > 3) {
            return [
                substr($string4, 0, 3),
                substr($string4, 3)
            ];
        } else {
            return [
                '',
                $string4
            ];
        }
    }


    public function callback(Request $request) {


        // check for payment


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => setting('my-fatoorah.url').'/GetPaymentStatus',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('Key' => $request->paymentId, 'KeyType' => 'paymentId'),
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.setting('my-fatoorah.secret_key'),

            ),
        ));

        $response = curl_exec($curl);


        $response = json_decode($response);


        if (strtolower($response->Data->InvoiceStatus) == strtolower("Paid")) {
            $order = Order::where('id',$response->Data->CustomerReference)->first();
            $order->payment_invoice = json_encode($response);
            $order->order_status = "PROCESSING";
            $order->save();
            session()->put('order_id',$order->id);
            session()->remove('coupon');
            session()->remove('payment_old_inputs');
            \Cart::clearCartConditions();
            \Cart::clear();

            return redirect()->to(route('success'));
        } else {
            $order = Order::where('id',$response->Data->CustomerReference)->first();
            $order->payment_invoice = json_encode($response);
            $order->save();
            $inputs = collect(json_decode(session('payment_old_inputs')))->toArray();
            alert()->error(trans('site.payment_error'), trans('site.error'));
            return \redirect()->to(route('show.checkout'))->withInput($inputs);
        }


    }
}
