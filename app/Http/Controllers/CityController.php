<?php

namespace App\Http\Controllers;

use App\Models\CountryCode;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate(
            ['country_code'=>'required']
        );

        // get all cities


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.myfatoorah.com/v2/GetCities?shippingMethod=1&countryCode='.$request->country_code,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(

                'Authorization: Bearer kVAb41ROAw5QJP58Qp6K80rXUVI9EA3iPZLHyJbSHn0D32yYwcDz9HD2SfgdRqq5mFuC_SfeGBudGvh4eznPvcPI_fSeuVqaLK_rDhUVg73JX_iy1nRnESAbjclQMdUrxwdpUIB7VXf_2XJMgKdEA0AEeAXkhDXXmW21b1yoeZyphUxO138a11KTAgJdOqEx3fUUxy_Y_2Glce2XyUhSmyU1p7xBOW6SwzGHTb5d66UMjozKSF2CSyZ97c-0RHxXn4CdgancWp2US0ADq9aqLOLaqP50viS3XXA5Lh-m8djLpOvxl2iHsoMbDIYGHLKlowG1lCTCnMmH4mxxI31sT9iMfoQsyJE02a22oeSObxA-AU67HotQ4t7kzQXVfXVUCVvzsLO_2XwP1DvTOsRXr-0NAXakgQI36ovmvod9c6cI7i19Zc3d2Tk_Az5s-ArlxhEgCewAvVCEAKt_DJyoiXoU3ENf1Jctr7glbnnNeO6Kafa2I7Cu4sXtagPUnkXUj9VhUdsmB95myjoqserBY0vB7mEU2cLaCNxrmnO2EaU-QCwgZYode89BcNb57IDM8rSAIHi2A1Za8SBBUYuUQjDnVTrE4ibM3wIMpIUwl7E9dpRvf1d6OEFeSVbNf3Edq2Hzmpz3WHJZEk7uecnzzs29vZL0dWvdjlMKIglXEyE10tlCDHIE8BocNlZSHXh9nETbjmcTf3mRUW8Zb4yrZJmq2T9ASi907XDjTiO9_zCrJQIo',

            ),
        ));
        try {
            $response = curl_exec($curl);
            curl_close($curl);
            $cities = json_decode($response)->Data->CityNames;
            setCountryData($request->country_code);
            $dial_code = session()->get('country')->dial_code;
            return response()->json(['cities'=>$cities,'dial_code'=>$dial_code]);

        } catch (\e $exception) {

        }


    }
}
