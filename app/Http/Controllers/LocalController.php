<?php

namespace App\Http\Controllers;

use Darryldecode\Cart\CartCondition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LocalController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($lang)
    {


        session()->put('local',$lang);
        \Cart::clearCartConditions();


        return redirect()->back();
    }
}
