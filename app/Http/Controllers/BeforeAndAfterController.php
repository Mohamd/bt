<?php

namespace App\Http\Controllers;

use App\BeforeAndAfter;
use Illuminate\Http\Request;

class BeforeAndAfterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $images = BeforeAndAfter::all();

        return view('before_after')->with(['images'=>$images]);
    }
}
