<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate(['ids'=>'required']);

        $ids = explode(',',$request->ids);

        $orders = Order::with('orderProducts')->whereIn('id',$ids)->get();
        return view('invoice')->with(['orders'=>$orders]);
    }
}
