<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use Carbon\Carbon;
use Darryldecode\Cart\Cart;
use Darryldecode\Cart\CartCondition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CouponController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {

        Session::remove('coupon',$request->coupon);
        \Cart::removeConditionsByType('coupon');
        $request->validate(['coupon'=>'required']);

        $coupon = Coupon::where('code','like',$request->coupon)->whereDate('from','<=',Carbon::now()->format('Y-m-d'))
            ->whereDate('to','>=', Carbon::now()->format('Y-m-d'))->first();


        if ($coupon) {

            $coupon->value = $coupon->type == 'percent' ? $coupon->value.'%' : $coupon->value;

            $condition = new CartCondition(array(
                'name' => 'Coupon '.$coupon->value,
                'type' => 'Coupon',
                'target' => 'subtotal', // this condition will be applied to cart's subtotal when getSubTotal() is called.
                'value' => '-'.$coupon->value,
                'order'=>1
            ));




            \Cart::condition($condition);

            Session::put('coupon',$request->coupon);
            return response()->json(['html'=>getTotals()->render(),'msg'=>trans('site.coupon_added_successfully')],200);
        } else {
            return response()->json(['msg'=>trans('site.coupon_not_valid')],404);
        }




    }
}
