<?php

namespace App\Http\Controllers;

use App\Models\CountryCode;
use Darryldecode\Cart\Cart;
use Darryldecode\Cart\CartCondition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CheckoutController extends Controller
{
    public function show() {
        $total_items = 0;
        foreach (\Cart::getContent() as $item) {
               $total_items += 1;
        }

        if ($total_items == 0) {
            alert()->warning(trans('site.empty_cart'));
            return redirect()->to('/');
        }

        $totals = getTotals()->render();

        $country_code = session()->get('country');

        $all_countries = '';

        if ($country_code->code != 'KW') {
            $all_countries  = $this->getAllCountries();

            $all_countries = collect($all_countries)->filter(function ($v) {
                return $v->CountryCode !== 'KW';
            });
        }

        return view('checkout',['totals'=>$totals,'country_code'=>$country_code,'all_countries'=>$all_countries]);
    }

    public function update(Request $request) {
        $request->validate([
           'item_id'=>'required',
           'qty'=>'required',
        ]);


        if ($request->qty <= 0) {
            \Cart::remove($request->item_id);
        } else {
            $cart_item = \Cart::get($request->item_id);

            $new_qty = $request->qty;


            \Cart::update($request->item_id,[
               'quantity'=> [
                'relative'=>false,
                'value'=>$new_qty
            ]

            ]);
        }

        if (session()->get('country')->code !== 'KW') {
            myfatoorah(['city'=>session()->get('city_name')]);
        }
        return response()->json(getTotals()->render(),200);
    }


    private function getAllCountries() {


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => setting('my-fatoorah.url').'/GetCountries',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.setting('my-fatoorah.secret_key'),

            ),
        ));

        try {
            $response = curl_exec($curl);

            curl_close($curl);
            $response = json_decode($response)->Data;
            return $response;
        } catch (\e $exception) {

        }


    }


}
