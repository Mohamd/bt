<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class ShippingController extends Controller
{
    private $shipping_orders_ids = [];
    public function index(Request  $request) {

        $orders = Order::whereIn('id',$request->ids)->with('orderProducts','orderProducts.product')->get();

        foreach ($orders as $order) {
            if ($order->order_status == 'PROCESSING') {

                $this->{(strtolower($order->shipping_method))}($order);
            }
        }

        if (count($this->shipping_orders_ids) > 0) {
            $this->dhl(null,true);
        }
        return response()->json('done',200);
    }

    public function wagon(Order $order) {



        $order->address = collect(json_decode($order->address))->toArray();
        $api_data = array(
            'email' => setting('wagon.email'),
            'password' => setting('wagon.password'),
            'secret_key' => setting('wagon.secret_key'),
            'pickup_area' => setting('wagon.pickup_area'),
            'pickup_block' => setting('wagon.pickup_block'),
            'pickup_address' => setting('wagon.pickup_address'),
            'pickup_street' => setting('wagon.pickup_street'),
            'pickup_latitude' => '-',
            'pickup_longitude' => '-',
            'drop_area' => $order->address['area'],
            'drop_block' => $order->address['block'],
            'drop_address' => $order->address['house'],
            'drop_street' => $order->address['street'],
            'drop_latitude' => '-',
            'drop_longitude' => '-',
            'receiver_name' => $order->name,
            'receiver_phone' => $order->phone,
            'shipment_package_name' => 'curly girls kw package',
            'shipment_package_value' => $order->total,
            'invoice_no' => $order->id);


        if (\request()->has('date_time')) {
            $date = explode(' ',\request()->date_time);
            $api_data['scheduled_date'] = Carbon::parse($date[0])->format('Y-m-d');
            $api_data['scheduled_time'] = Carbon::parse($date[0])->format('h:i:s');
        }


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => setting('wagon.wagon_url'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $api_data
        ));


        try {
            $response = curl_exec($curl);


            Log::channel('wagon')->info("order id -> $order->id" . "\n" . "response : $response");
            curl_close($curl);

            $response = json_decode($response);

            $order->tracking_code = $response->data->shipment_id;
            $order->order_status = "SHIPPING";

            $order->save();


        } catch (\Exception $e) {
            Log::channel('wagon')->info("order id -> $order->id" . "\n" . "response : $e");
        }



        return true;

    }

    public function dhl(Order $order) {

            $order_info = json_decode($order->payment_invoice);

            try {
                $data = [
                    "ShippingMethod"=>1,
                    "InvoiceNumbers"=> [
                        $order_info->Data->InvoiceId,
                    ],
                    "OrderStatusChangedTo"=> "1",
                ];


                Http::withHeaders([
                    'Authorization'=> 'Bearer '.setting('my-fatoorah.secret_key'),
                    'Content-Type'=> 'application/json',

                ])->post(setting('my-fatoorah.url').'/UpdateShippingStatus',$data);




                $response = Http::withHeaders([
                    'Authorization'=> 'Bearer '.setting('my-fatoorah.secret_key'),
                    'Content-Type'=> 'application/json',

                ])->get(setting('my-fatoorah.url').'/RequestPickup?shippingMethod=1');

                $response = json_decode($response->body());


                if ($response->Data[0]->OrderStatus == "RequestPickup" && $response->Data[0]->OrderNumber == $order_info->Data->InvoiceId) {
                    $order->order_status = "SHIPPING";
                    $order->save();
                }

            } catch (\Exception $e) {

            }

        }

}


