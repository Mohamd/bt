<?php

namespace App\Http\Controllers;

use Darryldecode\Cart\CartCondition;
use Illuminate\Http\Request;

class BoundController extends Controller
{

    public function __invoke(Request $request)
    {
        $request->validate(['bound'=>'required']);

        $bound = $request->bound;

        if ($bound == "kw") {
            setCountryData('KW');
        } else {
            setCountryData('SA');
        }


        \Cart::clearCartConditions();


        alert(trans('site.changed'));


        return redirect()->back();
    }
}
