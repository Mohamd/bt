<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    public $additional_attributes = ['full_name'];

    protected $table = 'order_products';
    public $timestamps = true;
    protected $fillable = array('order_id', 'product_id', 'total', 'quantity');

    public function product() {
        return $this->belongsTo(Product::class,'product_id');
    }



    public function getFullNameAttribute()
    {
        return 'name : '.$this->product()->first()->name . ' | ' .'quantity : '.$this->quantity.' | '.'price : ' . $this->total;

    }
}
