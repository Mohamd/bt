<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Product extends Model
{
    use Translatable;
    protected $translatable = ['name', 'description'];
    protected $table = 'products';
    public $timestamps = true;
    protected $fillable = array('name', 'price', 'new_price', 'image', 'image_kw', 'stock', 'width', 'height', 'length', 'depth');



}
