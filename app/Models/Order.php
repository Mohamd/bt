<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'orders';
    public $timestamps = true;
    protected $fillable = array('invoice_number', 'payment_invoice','tracking_code', 'name', 'phone', 'email', 'zone', 'city', 'postcode', 'address', 'notes', 'sub_total', 'coupon', 'coupon_price', 'shipping', 'total', 'payment_method', 'shipping_method', 'order_status_id');

    public function orderProducts() {
        return $this->hasMany(OrderProduct::class,'order_id');
    }

    public function scopeNotNull($query) {
        return $query->where('order_status','!=','NULL');
    }

    public function getFullAddressAttribute() {

        return $this->addressAttribute();
    }

    public function getAddressBrowseAttribute() {
        return $this->addressAttribute();
    }
    public function getAddressReadAttribute() {
        return $this->addressAttribute();
    }


    private function addressAttribute() {
        $address = json_decode($this->address);
        $new_address = "";
        if (!is_null($address)) {

            foreach ($address as $title=>$add) {
                $new_address .= $title . " : ". $add . "<br>";

            }


        } else {
            $new_address .= "Zone : ".$this->zone . "<br>";
            $new_address .= "City : ".$this->city . "<br>";
            $new_address .= "Address : ".$this->address . "<br>";
        }

        return $new_address;
    }

}
