<?php

use Darryldecode\Cart\CartCondition;

function setCountryData($country_code = '') {

    $country = \App\Models\CountryCode::where('code','=',$country_code)->first();

    \Illuminate\Support\Facades\Session::put('country',$country);
//    dd(\Illuminate\Support\Facades\Session::get('country_code'));
}

function getTotals() {

    $items = \Cart::getContent();

    $country_code = session()->get('country');

    if ($country_code->code == 'KW') {
        $condition = new CartCondition(array(
            'name' => trans('site.shipping_cost'),
            'type' => 'shipping',
            'target' => 'subtotal', // this condition will be applied to cart's subtotal when getSubTotal() is called.
            'value' => '1.5',
            'order'=>2,
        ));

        \Cart::condition($condition);
    }





    $cartConditions = \Cart::getConditions();



    $sub_total=\Cart::getSubTotal();

    $total = \Cart::getTotal();

    return view('component.total',['items'=>$items,'country_code'=>$country_code,'total'=>$total,'conditions'=>$cartConditions]);


}

// shipping
function myfatoorah($values) {

    if (!isset($values)) {
        return true;
    }
    session()->put('city_name',$values['city']);
    $curl = curl_init();
    $data = [];
    $data['ShippingMethod'] = "1";

    $items = [];
    foreach (\Cart::getContent() as $item) {

        $product_item = [];

        $product_item['ProductName'] = $item->associatedModel->name;
        $product_item['Description'] = $item->associatedModel->name;
        $product_item['Weight'] = $item->associatedModel->weight;
        $product_item['Width'] = $item->associatedModel->width;
        $product_item['Height'] = $item->associatedModel->height;
        $product_item['Depth'] = $item->associatedModel->depth;
        $product_item['Quantity'] = $item->quantity;
        $product_item['UnitPrice'] = $item->price;
        array_push($items,$product_item);
    }


    $data['items'] = $items;

    $data['CityName'] = $values['city'] ?? $values->city;
    $data['PostalCode'] = '';
    $data['CountryCode']=session()->get('country')->code;



    curl_setopt_array($curl, array(
        CURLOPT_URL =>setting('my-fatoorah.url'). '/CalculateShippingCharge',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.setting('my-fatoorah.secret_key'),
            'Content-Type: application/json',

        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    $response = json_decode($response);

    if (isset($response->Data->Fees)) {

        $condition = new CartCondition(array(
            'name' => trans('site.shipping_cost'),
            'type' => 'shipping',
            'target' => 'subtotal', // this condition will be applied to cart's subtotal when getSubTotal() is called.
            'value' => $response->Data->Fees,
            'order'=>2,
        ));

        \Cart::condition($condition);

        return response()->json(['html'=>getTotals()->render()],200);
    }

}
