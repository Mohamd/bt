$('#datetimepicker').datetimepicker();

$('#shipping_form').submit(function (e) {
    e.preventDefault();
    let inputs  =  $('input[name=row_id]:checked');
    let ids = [];
    let date_time = $('#datetimepicker').val();
    inputs.each(function (index) {
        ids.push($(this).val());
    })

    // $('#order_shipping_submit').attr('disabled','true');
    axios.post('/admin/shipping_request',{
        'ids':ids,
        date_time,
    }).then(res => {
        window.location.reload();
    }).catch(e => {
        $('#order_shipping_submit').attr('disabled','false');
    })



});


$(document).ready(function () {
    $('#print_shipping').submit(function (e) {
        let inputs  =  $('input[name=row_id]:checked');
        let ids = [];
        inputs.each(function (index) {
            ids.push($(this).val());
        })

        $('#ids').val(ids);

    });

})

