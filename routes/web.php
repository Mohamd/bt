<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController')->name('/');

Route::get('local/{lang}','LocalController')->name('local');
Route::get('checkout','CheckoutController@show')->name('show.checkout');
Route::post('checkout/update','CheckoutController@update')->name('update.checkout');

Route::post('setProduct','CheckoutController@setProduct')->name('set.checkout');

Route::resource('cart','CartController');

Route::resource('order','OrderController');

Route::any('callback','OrderController@callback')->name('call_back');

Route::post('coupon','CouponController')->name('coupon');

Route::get('cities','CityController')->name('cities');
Route::post('calcShipping','CalcShippingController@index')->name('calcShipping');
Route::view('success','success')->name('success');
Route::view('product','product');

Route::get('bound','BoundController')->name('bound');
Route::get('page/{slug}','PageController')->name('page');

Route::get('before-and-after','BeforeAndAfterController')->name('before_after');



Route::group(['prefix' => 'admin'], function () {
    Route::post('shipping_request','ShippingController@index')->name('shipping.request');
    Route::post('print_invoices','InvoiceController')->name('print_invoices');
    Voyager::routes();
});


