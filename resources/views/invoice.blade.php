<!DOCTYPE html>

<html dir="rtl" lang="ar">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>الفاتورة </title>


    <link href="{{asset('catalog/view/javascript/bootstrap/css/bootstrap.css')}}" rel="stylesheet" media="all">
    <script type="text/javascript" src="{{asset('catalog/view/javascript/jquery/jquery-2.1.1.min.js')}}"></script>
{{--    <script type="text/javascript" src="./الفاتورة رقم_files/bootstrap.min.js.download"></script>--}}

    <link type="text/css" href="{{asset('stylesheet.css')}}" rel="stylesheet" media="all">
</head>

<body>
<div class="container">
    @foreach($orders as $order)
    <div style="page-break-after: always;">
        <h1>الفاتورة رقم #{{$order->invoice_number}}</h1>
        <table class="table table-bordered">
            <thead>
            <tr>
                <td colspan="2">تفاصيل الطلب</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td style="width: 50%;">
                    <address>
                        <strong>{{setting('site.name')}}</strong><br>
                        Rimuathya<br>
                        Block 6<br>
                        St shaheen alghanim
                    </address>
                    <b>البريد الالكتروني:</b> admin@curlygirlskw.com<br>
                    <b>الموقع الالكتروني:</b> <a href="{{route('/')}}">{{route('/')}}</a>
                </td>
                <td style="width: 50%;"><b>تاريخ الاضافة
                        :{{$order->created_at->format('Y/m/d')}}
                    </b><br>
                    <b>رقم الطلب:</b> {{$order->id}}<br>
                    <b>طريقة الدفع:</b> {{$order->payment_method}}<br>
                    <b>طريقة الشحن:</b>{{$order->shipping_method}}<br>

                    <b>اسم العميل :{{$order->name}}</b>
                    <br>
                    <b>رقم هاتف العميل : {{$order->phone}}</b>
                </td>

            </tr>
            </tbody>
        </table>


        <table class="table table-bordered">
            <thead>
            <tr>

                <td style="width: 100%;"><b>العنوان</b></td>

            </tr>
            </thead>
            <tbody>
            <tr>

                <td>
                    <address>
                        {!! $order->full_address !!}
                    </address>
                </td>
            </tr>
            </tbody>
        </table>
        <table class="table table-bordered">
            <thead>
            <tr>
                <td><b>الطلبات</b></td>

                <td class="text-right"><b>الكمية</b></td>
                <td class="text-right"><b>سعر الوحدة</b></td>
                <td class="text-right"><b>الاجمالي</b></td>
            </tr>
            </thead>
            <tbody>

            @foreach ($order->orderProducts as $product)
            <tr>
                <td>{{$product->product->name}}
                </td>

                <td class="text-right">{{$product->quantity}}</td>
                <td class="text-right"> {{number_format($product->total,2)}} K.D</td>
                <td class="text-right"> K.D {{number_format($product->total * $product->quantity,2)}}</td>
            </tr>
            @endforeach

            <tr>
                <td class="text-right" colspan="4"><b>رسوم الشحن</b></td>
                <td class="text-right"> K.D {{$order->shipping}}</td>
            </tr>

            @if (!is_null($order->coupon))
                <tr>
                    <td class="text-right" colspan="4"><b>خصم الكوبون</b></td>
                    <td class="text-right"> K.D {{number_format($order->coupon_price,2)}}</td>
                </tr>

            @endif
            <tr>
                <td class="text-right" colspan="4"><b>المجموع</b></td>
                <td class="text-right"> K.D {{number_format($order->total,2)}}</td>
            </tr>
            </tbody>
        </table>

    </div>
    @endforeach

</div>


</body>

</html>
