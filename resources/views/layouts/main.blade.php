<!DOCTYPE html>
<html dir="rtl" lang="ar">



<head>
    @routes
    <meta charset="UTF-8" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <title>Curly Girls </title>
    <base />
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description" />
    <meta content="" name="keywords" />
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->

    @if(\Illuminate\Support\Facades\App::getLocale() == 'ar')
    <link href="{{asset('catalog/view/javascript/soconfig/css/bootstrap/bootstrap.rtl.min.css')}}" rel="stylesheet">
    <link href="{{asset('catalog/view/javascript/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('catalog/view/javascript/soconfig/css/lib.css')}}" rel="stylesheet">
    <link href="{{asset('catalog/view/theme/so-claue/css/ie9-and-up.css')}}" rel="stylesheet">
    <link href="{{asset('catalog/view/theme/so-claue/css/custom.css')}}" rel="stylesheet">
    <link href="{{asset("catalog/view/javascript/pe-icon-7-stroke/css/pe-icon-7-stroke.css")}}" rel="stylesheet">
    <link href='{{asset('catalog/view/javascript/pe-icon-7-stroke/css/helper.css')}}' rel="stylesheet">
    <link href="{{asset("catalog/view/javascript/so_page_builder/css/style_render_226.css")}} rel="stylesheet">
    <link href="{{asset("catalog/view/javascript/so_page_builder/css/style.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/javascript/so_page_builder/css/style_render_228.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/javascript/so_countdown/css/style.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/javascript/so_megamenu/so_megamenu.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/javascript/so_megamenu/wide-grid.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/javascript/soconfig/css/owl.carousel.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/javascript/so_sociallogin/css/so_sociallogin.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/theme/so-claue/css/layout7/main-rtl.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/theme/so-claue/css/header/header7-rtl.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/theme/so-claue/css/footer/footer7-rtl.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/theme/so-claue/css/responsive-rtl.css")}}" rel="stylesheet">
    @else
    <link href="{{asset('catalog/view/javascript/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('catalog/view/javascript/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('catalog/view/javascript/soconfig/css/lib.css')}}" rel="stylesheet">
    <link href="{{asset('catalog/view/theme/so-claue/css/ie9-and-up.css')}}" rel="stylesheet">
    <link href="{{asset('catalog/view/theme/so-claue/css/custom.css')}}" rel="stylesheet">
    <link href="{{asset("catalog/view/javascript/pe-icon-7-stroke/css/pe-icon-7-stroke.css")}}" rel="stylesheet">
    <link href='{{asset('catalog/view/javascript/pe-icon-7-stroke/css/helper.css')}}' rel="stylesheet">
    <link href="{{asset("catalog/view/javascript/so_page_builder/css/style_render_226.css")}} rel="stylesheet">
    <link href="{{asset("catalog/view/javascript/so_page_builder/css/style.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/javascript/so_page_builder/css/style_render_228.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/javascript/so_countdown/css/style.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/javascript/so_megamenu/so_megamenu.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/javascript/so_megamenu/wide-grid.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/javascript/soconfig/css/owl.carousel.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/javascript/so_sociallogin/css/so_sociallogin.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/theme/so-claue/css/layout7/main.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/theme/so-claue/css/header/header7.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/theme/so-claue/css/footer/footer7.css")}}" rel="stylesheet">
    <link href="{{asset("catalog/view/theme/so-claue/css/responsive.css")}}" rel="stylesheet">
    @endif
    <link href="http://fonts.googleapis.com/css?family=Poppins:400,400i,500,600,700,&amp;subset=latin" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Libre%20Baskerville:400,400i,500,600,700,&amp;subset=latin-ext"
        rel="stylesheet">
    <style type="text/css">
        body {
            font-family: 'Poppins', sans-serif;
            font-size: 14px !important;
            font-weight: 400 !important;
        }

        .font-custom,
        .common-home #content .module .form-group,
        .sohomepage-slider .inner-text h5 {
            font-family: 'Libre Baskerville', sans-serif;
            font-weight: 400 !important;
        }
    </style>
    <style>
        #payment-confirm-button {
            display: none;
        }
    </style>
    <style>
        #xlarg-logo img {
            max-width: 50%;
        }
    </style>
    <link href="{{asset("image/catalog/website1000px_icon.png")}}" rel="icon" />

    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />


    @stack('css')
</head>
{{--rtl--}}
<body class="common-home  layout-7">

@include('sweet::alert')
    <!-- header -->

        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="brand">
                        <a href="/"><img alt="Curly Girls" class="lazyload" data-sizes="auto"
                                         data-src="{{asset('logo.png')}}"
                                         width="100"
                                         height="50"
                                         src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                         title="Curly Girls" /></a>
                        <br>
                        <div class="row">
                            <div  class="brand_icons container">
                                <div>
                                    <a href="{{route('show.checkout')}}" style="font-size:24px "><i class="fa fa-shopping-cart"></i></a>
                                </div>

                                <div >
                                    <button class="btn btn-success btn-sm" >
                                        <a style="color:white" href="{{route('bound',['bound'=>"false"])}}" target="_self">

                                            <span>{{trans('site.out_country')}}</span>
                                        </a>

                                    </button>

                                </div>

                                <div >
                                    <button class="btn btn-warring btn-sm" >
                                        <a style="color:white" href="{{route('bound',['bound'=>"kw"])}}" target="_self" >

                                            <span>{{trans('site.from_country')}}</span>
                                        </a>

                                    </button>

                                </div>

                                <div >
                                    <button class="btn  btn-sm" >
                                        <a style="color:white" href="{{route('local',['lang'=>\Illuminate\Support\Facades\App::getLocale() == 'ar' ? 'en':'ar'])}}" target="_self" >

                                            <span>

                                                {{\Illuminate\Support\Facades\App::getLocale() == 'ar' ? 'ENGLISH':'العربية'}}
                                            </span>
                                        </a>

                                    </button>

                                </div>


                            </div>

                        </div>
                    </div>


                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <ul class="nav navbar-nav">
                        <li class="logo_nav">
                            <a href="/"><img alt="Curly Girls" class="lazyload" data-sizes="auto"
                                             data-src="{{asset('logo.png')}}"
                                             width="200"
                                             height="100"
                                             src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                             title="Curly Girls" /></a>

                        </li>
                        <li id="my_cart"><a href="{{route('show.checkout')}}" style="font-size:24px "><i class="fa fa-shopping-cart"></i></a></li>
                        <li>
                            <a  href="{{route('local',['lang'=>\Illuminate\Support\Facades\App::getLocale() == 'ar' ? 'en':'ar'])}}" target="_self" >

                                            <span>

                                                {{\Illuminate\Support\Facades\App::getLocale() == 'ar' ? 'ENGLISH':'العربية'}}
                                            </span>
                            </a>
                        </li>
                        <div class="nav_menu">
                            {{ menu('nav') }}


                        </div>
                        <li>
                            <a href="{{route('before_after')}}">{{trans('site.before_and_after')}}</a>
                        </li>



{{--                        <li class="@if (request()->is('/')) active @endif"><a href="/">{{trans('site.home_page')}} </a></li>--}}

{{--                        <li><a href="#">Link</a></li>--}}

                    </ul>


                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>


    <!-- end header -->

{{--    <header class=" variant typeheader-7" id="header">--}}

{{--        <div class="header-top">--}}
{{--            <div class="row  hidden-md hidden-lg">--}}
{{--                <ul class="top-link list-inline lang-curr">--}}
{{--                    <li class="currency">--}}
{{--                        ar--}}
{{--                        <div class="pull-left">--}}

{{--                        </div>--}}

{{--                    </li>--}}
{{--                    <li class="instagram"><a href="https://www.instagram.com/curlygirls_kw/" target="_blank"><i--}}
{{--                                class="fa fa-instagram"></i></a></li>--}}
{{--                    <li class="whatsapp"><a href="https://api.whatsapp.com/message/FPUJ37KUCTYEB1" target="_blank"><i--}}
{{--                                class="fa fa-whatsapp"></i></a></li>--}}
{{--                    <li class="cart">--}}
{{--                        <a class="btn-group top_cart dropdown-toggle" data-loading-text="جاري التحميل ... "--}}
{{--                            data-toggle="dropdown">--}}
{{--                            <div class="shopcart">--}}
{{--                                <span class="icon-c">--}}

{{--                                    <svg width="20" height="20" class="icon-shopping-handbag">--}}
{{--                                        <svg id="icon-shopping-handbag" viewBox="0 0 1024 1056">--}}
{{--                                            <path--}}
{{--                                                d="M1023 959l-84-504q-5-49-44-84t-88-35h-71v-85q0-48-16.5-91.5t-46-75.5-71-50.5T513 15q-64 0-115.5 32T317 133t-29 118v85h-77q-16 0-32 4.5t-30 12-26 18.5-21 24-15 28-8 30L2 958q-5 40 15 62 19 21 54 21h873q23 0 38-7t24-17q20-23 17-58zM352 251q0-72 45.5-122T513 79q35 0 65 13.5t50.5 37 32 55T672 251v85H352v-85zm595 725l-872 1q-12 0-10-11l77-504q2-12 8-23.5t15.5-20T187 405t24-5h77v73q-7 5-13 10.5T265 496t-6.5 15.5T256 528q0 27 18.5 45.5T320 592t45.5-18.5T384 528q0-36-32-55v-73h320v73q-32 19-32 55 0 27 18.5 45.5T704 592t45.5-18.5T768 528q0-36-32-55v-73h71q6 0 12 1.5t12 4 11.5 6 10 7.5 8.5 9 7 11 5 12 3 13l83 503q1 4-2 6.5t-10 2.5z">--}}
{{--                                            </path>--}}
{{--                                        </svg>--}}
{{--                                    </svg>--}}
{{--                                </span>--}}

{{--                            </div>--}}
{{--                        </a>--}}

{{--                    </li>--}}
{{--                </ul>--}}
{{--            </div>--}}

{{--            <div class="row  hidden-md hidden-lg">--}}
{{--                <div class="logo">--}}
{{--                    <a href="index9328.html?route=common/home"><img alt="Curly Girls" class="lazyload" data-sizes="auto"--}}
{{--                            data-src="http://curlygirlskw.com/image/catalog/website1000px.png"--}}
{{--                            src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="--}}
{{--                            title="Curly Girls" /></a>--}}


{{--                </div>--}}

{{--            </div>--}}

{{--            <div class="row">--}}

{{--                <div class="col-lg-12 col-md-4 col-sm-4 hidden-xs" style="display: flex;justify-content: center">--}}

{{--                    <div class="telephone row" style="display: flex;justify-content: center">--}}
{{--                        <ul class="socials text-center">--}}
{{--                            <li class="instagram"><a href="https://www.instagram.com/curlygirls_kw/" target="_blank"><i--}}
{{--                                        class="fa fa-instagram"></i></a></li>--}}
{{--                            <li class="whatsapp"><a href="https://api.whatsapp.com/message/FPUJ37KUCTYEB1"--}}
{{--                                    target="_blank"><i class="fa fa-whatsapp"></i></a></li>--}}

{{--                            <br>--}}
{{--                            <li class="language">--}}
{{--                                <div class="pull-left">--}}
{{--                                    <form action="http://curlygirlskw.com/index.php?route=common/language/language"--}}
{{--                                        enctype="multipart/form-data" id="form-language" method="post">--}}
{{--                                        <div class="btn-group">--}}
{{--                                            <button class="btn-link dropdown-toggle" data-toggle="dropdown">--}}

{{--                                                <span class="hidden-xs hidden-sm hidden-md">عربي</span>--}}
{{--                                                <i class="fa fa-angle-down"></i>--}}
{{--                                            </button>--}}

{{--                                            <ul class="dropdown-menu">--}}
{{--                                                <li>--}}
{{--                                                    <button class="btn-block language-select" name="en-gb"--}}
{{--                                                        type="button">--}}
{{--                                                        English--}}
{{--                                                    </button>--}}
{{--                                                </li>--}}
{{--                                                <li>--}}
{{--                                                    <button class="btn-block language-select" name="ar" type="button">--}}
{{--                                                        عربي--}}
{{--                                                    </button>--}}
{{--                                                </li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                        <input name="code" type="hidden" value="" />--}}
{{--                                        <input name="redirect" type="hidden" value="index9328.html?route=common/home" />--}}
{{--                                    </form>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}


{{--                </div>--}}

{{--                <div class="header-top-left col-lg-12 col-md-4 col-sm-4 col-xs-6">--}}


{{--                    <div class="" id="my_account">--}}
{{--                        <ul class="dropdown-menu">--}}


{{--                            <li><a href="index5502.html?route=account/register">تسجيل جديد</a></li>--}}
{{--                            <li><a href="indexe223.html?route=account/login">تسجيل الدخول</a></li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}

{{--                </div>--}}

{{--            </div>--}}

{{--        </div>--}}


{{--        <div class="header-middle">--}}

{{--            <div class="row">--}}

{{--                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
{{--                    <div class="main-menu-w">--}}


{{--                        <div class="responsive megamenu-style-dev" id="so_megamenu_85">--}}

{{--                            <nav class="navbar-default">--}}
{{--                                <div class=" container-megamenu   horizontal ">--}}
{{--                                    <div class="navbar-header">--}}
{{--                                        <button class="navbar-toggle" data-toggle="collapse" id="show-megamenu-88"--}}
{{--                                            type="button">--}}
{{--                                            <span class="icon-bar"></span>--}}
{{--                                            <span class="icon-bar"></span>--}}
{{--                                            <span class="icon-bar"></span>--}}
{{--                                        </button>--}}
{{--                                    </div>--}}

{{--                                    <div class="megamenu-wrapper">--}}

{{--                                        <span class="pe-7s-close icon-close" id="remove-megamenu-73"></span>--}}

{{--                                        <div class="megamenu-pattern">--}}
{{--                                            <ul class="megamenu" data-animationtime="500" data-transition="slide">--}}
{{--                                                <li class="home">--}}
{{--                                                    <a href="index9328.html?route=common/home">--}}
{{--                                                        <span><strong>الصفحة الرئيسية</strong></span>--}}
{{--                                                    </a>--}}
{{--                                                </li>--}}



{{--                                                <li class="order_from_Kuwait">--}}
{{--                                                    <p class='close-menu'></p>--}}
{{--                                                    <a class="clearfix" href="">--}}
{{--                                                        <strong>--}}
{{--                                                            <i class="fa fa-user"></i>حسابي--}}
{{--                                                        </strong>--}}

{{--                                                    </a>--}}

{{--                                                </li>--}}

{{--                                                <li class="order_from_Kuwait">--}}
{{--                                                    <p class='close-menu'></p>--}}
{{--                                                    <a class="clearfix"--}}
{{--                                                        href="index1d41.html?route=common/bound&amp;in_bound=true">--}}
{{--                                                        <strong>--}}
{{--                                                            <i class="fa fa-home"></i>الطلب من داخل الكويت--}}
{{--                                                        </strong>--}}

{{--                                                    </a>--}}

{{--                                                </li>--}}


{{--                                                <li class="order_out_Kuwait">--}}
{{--                                                    <p class='close-menu'></p>--}}
{{--                                                    <a class="clearfix"--}}
{{--                                                        href="index07db.html?route=common/bound&amp;in_bound=false">--}}
{{--                                                        <strong>--}}
{{--                                                            <i class="fa fa-plane "></i>الطلب من خارج الكويت--}}
{{--                                                        </strong>--}}

{{--                                                    </a>--}}

{{--                                                </li>--}}


{{--                                                <li class="">--}}
{{--                                                    <p class='close-menu'></p>--}}
{{--                                                    <a class="clearfix"--}}
{{--                                                        href="index8816.html?route=information/information&amp;information_id=4">--}}
{{--                                                        <strong>--}}
{{--                                                            <i class="fa fa-id-card "></i>من نحن--}}
{{--                                                        </strong>--}}

{{--                                                    </a>--}}

{{--                                                </li>--}}


{{--                                                <li class="">--}}
{{--                                                    <p class='close-menu'></p>--}}
{{--                                                    <a class="clearfix"--}}
{{--                                                        href="index11cd.html?route=information/information&amp;information_id=7">--}}
{{--                                                        <strong>--}}
{{--                                                            <i class="fa fa-bell"></i>كيفية إستعمال المنتج--}}
{{--                                                        </strong>--}}

{{--                                                    </a>--}}

{{--                                                </li>--}}


{{--                                                <li class="">--}}
{{--                                                    <p class='close-menu'></p>--}}
{{--                                                    <a class="clearfix"--}}
{{--                                                        href="index7184.html?route=information/information&amp;information_id=8">--}}
{{--                                                        <strong>--}}
{{--                                                            <i class="fa fa-home"></i>فوائد المنتج--}}
{{--                                                        </strong>--}}

{{--                                                    </a>--}}

{{--                                                </li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </nav>--}}
{{--                        </div>--}}


{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="bottom-right col-lg-12 hidden-md col-sm-12 col-xs-12">--}}
{{--                    <div class="hidden-sm hidden-xs welcome-msg">--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--            </div>--}}

{{--        </div>--}}

{{--    </header>--}}


{{--    <div class="hidden-sm hidden-xs d-flex align-items-center justify-content-center" style="text-align:center">--}}
{{--        <div class="logo">--}}
{{--            <div id="xlarg-logo">--}}


{{--                <a href="/"><img alt="Curly Girls" class="lazyload" data-sizes="auto" data-src="website1000px.png"--}}
{{--                        src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="--}}
{{--                        title="Curly Girls" /></a>--}}


{{--            </div>--}}
{{--        </div>--}}


{{--    </div>--}}

    <!-- end header -->

    <!-- start content -->
    <div id="app">
        <div class="" id="content" >


            @yield('content')
            <!-- start footer-->
                <footer id="my_footer" class="">
                    <div class="container">
                        <p>{{trans('site.copyright',["name"=>'CurlyGirls',"year"=>\Carbon\Carbon::now()->format('Y')])}} </p>
                    </div>

                </footer>
                <!-- end footer -->
        </div>


    </div>





    <!-- end content -->





    <!-- scripts -->
    <script src="{{asset("catalog/view/javascript/jquery/jquery-2.1.1.min.js")}}"></script>
    <script src="{{asset("catalog/view/javascript/bootstrap/js/bootstrap.min.js")}}"></script>
    <script src="{{asset("catalog/view/javascript/soconfig/js/libs.js")}}"></script>
    <script src="{{asset("catalog/view/javascript/soconfig/js/so.system.js")}}"></script>
    <script src="{{asset("catalog/view/javascript/soconfig/js/jquery.sticky-kit.min.js")}}"></script>
    <script src="{{asset("catalog/view/javascript/lazysizes/lazysizes.min.js")}}"></script>
    <script src="{{asset("catalog/view/javascript/soconfig/js/toppanel.js")}}"></script>
    <script src="{{asset("catalog/view/javascript/soconfig/js/isotope.pkgd.min.js")}}"></script>
    <script src="{{asset("catalog/view/javascript/soconfig/js/imagesloaded.pkgd.min.js")}}"></script>
    <script src="{{asset("catalog/view/theme/so-claue/js/so.custom.js")}}"></script>
    <script src="{{asset("catalog/view/theme/so-claue/js/common.js")}}"></script>
    <script src="{{asset("catalog/view/javascript/so_page_builder/js/section.js")}}"></script>

<script src="{{asset("catalog/view/javascript/soconfig/js/lightslider.js")}}"></script>

    <script src="{{asset("catalog/view/javascript/so_page_builder/js/modernizr.video.js")}}"></script>
    <script src="{{asset("catalog/view/javascript/so_page_builder/js/swfobject.js")}}"></script>
{{--    <script src="{{asset("catalog/view/javascript/so_page_builder/js/video_background.js")}}"></script>--}}
    <script src="{{asset("catalog/view/javascript/so_countdown/js/jquery.cookie.js")}}"></script>
    <script src="{{asset("catalog/view/javascript/so_megamenu/so_megamenu.js")}}"></script>
    <script src="{{asset("catalog/view/javascript/soconfig/js/owl.carousel.js")}}"></script>

    <script src="{{asset("js/app.js")}}"></script>


<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script defer>
    $(document).ready(function () {
        $('select').select2();
    })
</script>

@stack('js')

</body>

</html>
