@extends('layouts.main')
@push('css')
    <link rel="stylesheet" href="{{asset('so_onepagecheckout.css')}}">
@endpush
@section('content')
    <form method="post" action="{{route('order.store')}}">
        @csrf
    <div class="col-sm-12">
        <div class="so-onepagecheckout layout1 ">
            <div class="col-left col-lg-6 col-md-6 col-sm-6 col-xs-12">




                <div class="checkout-content checkout-register">

                    <fieldset id="account">
                        <h2 class="secondary-title"><i class="fa fa-user-plus"></i>{{trans('site.personal_information')}}</h2>
                        <div class="payment-new box-inner">


                            <div class="form-group input-firstname required" style="width: 100%; float: left;">
                                <input type="text" name="name" value="{{old('name')}}" placeholder="{{trans('validation.attributes.name')}}" id="input-payment-firstname"
                                       class="form-control" required>
                            </div>


                            <div class="form-group required">
                                <input type="text" name="email" value="{{old('email')}}" placeholder="{{trans('validation.attributes.email')}}" id="input-payment-email"
                                       class="form-control" required>
                            </div>


                        </div>
                    </fieldset>

                    <fieldset id="address">

                        <h2 class="secondary-title"><i class="fa fa-map-marker"></i>{{trans('site.shipping_information')}}</h2>


                        <div class=" checkout-payment-form">

                            <div class="box-inner">
                                @include('component.loader')
                                    @if ($country_code->code == 'KW')
                                    <div class="form-group required">
                                        <input type="text" id="dial_code" value="{{$country_code->dial_code}}"
                                               disabled required class="text-center" style="width: 30%!important;border-right: 1px solid black;direction:ltr">
                                        <input type="text"  name="phone" value="{{old('phone')}}" placeholder="{{trans('validation.attributes.phone')}}"
                                               class="" style="width: 70%!important" required>


                                    </div>


                                        <!--area -->
                                    <div class="form-group required" >
                                        <input type="text" name="area"  value="{{old('area')}}" placeholder="{{trans('site.area')}}"
                                                class="form-control" required>
                                    </div>

                                        <!-- end area -->

                                            <!-- block  -->
                                            <div class="form-group required" >
                                                <input type="text" name="block" value="{{old('block')}}"  placeholder="{{trans('site.block')}}"
                                                       class="form-control" required>
                                            </div>
                                            <!-- end block -->

                                            <!--  street -->
                                            <div class="form-group required" >
                                                <input type="text" name="street" value="{{old('street')}}"  placeholder="{{trans('site.street')}}"
                                                       class="form-control" required>
                                            </div>
                                            <!--  end street -->

                                        <!-- house  -->
                                            <div class="form-group required" >
                                                <input type="text" name="house" value="{{old('house')}}" placeholder="{{trans('site.house')}}"
                                                       class="form-control" required>
                                            </div>
                                        <!-- end house -->
                                    @else
                                            <div class="form-group required" >
                                                    <select class="form-control" name="zone" id="zone" required>
                                                        <option selected disabled>{{trans('site.zone')}}</option>
                                                        @foreach($all_countries as $country)
                                                            <option value="{{$country->CountryCode}}"

                                                            >{{$country->CountryName}}</option>
                                                        @endforeach

                                                    </select>

                                            </div>

                                            <div class="form-group required" >
                                                <select class="form-control" name="city" id="city" required>
                                                    <option selected disabled>{{trans('validation.attributes.city')}}</option>
                                                </select>

                                            </div>

                                    <div class="form-group required">
                                        <input type="text" id="dial_code" value="+"
                                               disabled required class="text-center" style="width: 30%!important;border-right: 1px solid black;">
                                        <input type="text"  name="phone" value="{{old('phone')}}" placeholder="{{trans('validation.attributes.phone')}}"
                                               class="" style="width: 70%!important" required>


                                    </div>


                                    <!--area -->
                                            <div class="form-group required" >
                                                <input type="text" name="postcode"  value="{{old('postcode')}}" placeholder="{{trans('site.postcode')}}"
                                                       class="form-control" >
                                            </div>

                                            <!--area -->
                                            <div class="form-group required" >
                                                <input type="text" name="address"  value="{{old('address')}}" placeholder="{{trans('validation.attributes.address')}}"
                                                       class="form-control" required>
                                            </div>

                                        @endif

                            </div>
                        </div>


                    </fieldset>





                </div>

            </div>

            <div class="col-right col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <section class="section-left">
                    @if (session()->get('country')->code == 'KW')
                    <div class="ship-payment">
                        <div class="checkout-content checkout-shipping-methods">

                            <h2 class="secondary-title"><i class="fa fa-location-arrow"></i>
                                {{trans('site.payment_method')}}
                            </h2>
                            <div class="box-inner">
                                <div class="form-group">
                                    <input type="radio" name="payment_method"  value="CASH ON DELIVERY"
                                           checked>
                                    <span><strong>{{trans('site.CASH_ON_DELIVERY')}}</strong></span>


                                </div>

                                <div class="form-group">
                                    <input type="radio"  name="payment_method" value="Credit Card"
                                           >
                                    <span><strong>{{trans('site.knet_visa')}}</strong></span>


                                </div>


                            </div>
                        </div>





                    </div>
                    @endif


                </section>
                <section class="section-right">
                    <div id="coupon_voucher_reward">
                        <div class="checkout-content coupon-voucher">
                            <h2 class="secondary-title"><i class="fa fa-gift"></i>{{trans('site.coupon')}}</h2>
                            <div class="box-inner">
                                <div class="panel-body checkout-coupon">
                                    <label class="col-sm-2 control-label" for="input-coupon">{{trans('site.coupon')}}</label>
                                    <div class="input-group">
                                        <input type="text"  value="{{old('coupon') ?? session()->get('coupon')}}" placeholder="{{trans('site.coupon')}}" id="input-coupon"
                                               class="form-control">
                                        <span class="input-group-btn">
                                    <input type="button"  id="button-coupon"

                                           value="{{trans('site.apply_coupon')}}"
                                           class="btn-primary button">
                                </span>
                                    </div>
                                </div>



                            </div>
                        </div>

                    </div>
                    <div id="payment">
                        @include('component.loader')
                        <div id="totals">
                            {!! $totals !!}
                        </div>
                    </div>







                    <div class="checkout-content confirm-section">
                        <div>
                            <h2 class="secondary-title"><i class="fa fa-comment"></i>{{trans('site.notes')}}</h2>
                            <label>
                                <textarea name="notes" rows="8" class="form-control " value="{{old('notes')}}"></textarea>
                            </label>
                        </div>



                        <div class="confirm-order">
                            <button id="so-checkout-confirm-button"
                                    class="btn btn-primary button confirm-button">
                                {{trans('site.submit_order')}}
                            </button>
                        </div>
                    </div>
                </section>
            </div>
        </div>

    </div>
    </form>
@endsection
