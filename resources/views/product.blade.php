@extends('layouts.main')

@section('content')

    <div class="content-main container product-detail  ">
        <div class="row">





            <div id="content" class="product-view col-md-9 col-sm-12 col-xs-12 fluid-sidebar">




                <div class="content-product-mainheader clearfix">
                    <div class="row">
                        <div class="content-product-left  col-md-5 col-sm-12 col-xs-12">
                            <div class="so-loadeding"></div>

                            <div class="large-image  ">
                                <img itemprop="image" class="product-image-zoom lazyload" data-sizes="auto"
                                     src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                     data-src="http://placehold.it/500/500"

                                     title="جل بذرة الكتان" alt="جل بذرة الكتان" />
                            </div>

                            <div id="thumb-slider" class="full_slider  contentslider" data-rtl="yes"
                                 data-autoplay="no" data-pagination="no" data-delay="4" data-speed="0.6"
                                 data-margin="10" data-items_column0="4" data-items_column1="3"
                                 data-items_column2="5" data-items_column3="3" data-items_column4="2"
                                 data-arrows="yes" data-lazyload="yes" data-loop="no" data-hoverpause="yes">


                            </div>



                        </div>

                        <div class="content-product-right col-md-7 col-sm-12 col-xs-12">
                            <div class="title-product">
                                <h1 style="font-size: 24px;">جل بذرة الكتان</h1>
                            </div>


                            <div class="product_page_price price" style="color:black!important;"
                                 itemprop="offerDetails" itemscope itemtype="http://data-vocabulary.org/Offer">
									<span class="price-new" style="color:black!important;"><span itemprop="price"
                                                                                                 id="price-old"> K.D 4.00</span></span>



                            </div>








                            <div id="product">

                                <div class="box-cart clearfix form-group">

                                    <div class="form-group box-info-product">
                                        <div class="option quantity">
                                            <label class="control-label" for="input-quantity">الكمية:</label>
                                            <div class="input-group quantity-control">
													<span
                                                        class="input-group-addon product_quantity_down fa fa-minus"></span>
                                                <input class="form-control" type="text" name="quantity" value="1" />
                                                <input type="hidden" name="product_id" value="50" />
                                                <span
                                                    class="input-group-addon product_quantity_up fa fa-plus"></span>
                                            </div>
                                        </div>
                                        <div class="detail-action">
                                            <div class="cart">
                                                <div class="form-group">
                                                    <input type="button" style="background-color: gray; "
                                                           value="إضافة إلى الحقيبة"
                                                           data-loading-text="جاري التحميل ..." id="button-cart"
                                                           class="btn btn-mega " />
                                                </div>
                                                <div class="form-group">
                                                    <input type="button"
                                                           style="background-color:#c8a9c8!important;margin: 0;"
                                                           value="الشراء الأن" data-loading-text="جاري التحميل ..."
                                                           id="button-checkout" class="btn btn-checkout btn-mega " />
                                                </div>



                                            </div>

                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>



                            </div>

                        </div>
                    </div>
                </div>

                <div class="content-product-mainbody clearfix row">


                    <div class="content-product-content col-sm-12">
                        <div class="content-product-midde clearfix">

                            <div class="producttab ">
                                <div class="tabsslider   horizontal-tabs  col-xs-12">


                                    <div class="tab-content  col-xs-12">
                                        <div class="tab-pane active" id="tab-description">


                                            <h3 class="product-property-title"> وصف المنتج</h3>
                                            <div id="collapse-description" class="desc-collapse showup">

                                            </div>

                                        </div>






                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>

                </div>


            </div>
        </div>



    </div>
@endsection
