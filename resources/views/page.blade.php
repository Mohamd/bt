@extends('layouts.main')

@section('content')
    <div class="m-4 p-4">
        <div class="container">
            <div class="products_header d-flex justify-content-center align-items-center">
                <h2 class="text-center">{{$page->translate()->title}}</h2>
            </div>

            <div class="row" style="min-height: 60vh">
                <div class="container">
                    {!! $page->translate()->content !!}
                </div>

            </div>
        </div>
    </div>
@endsection
