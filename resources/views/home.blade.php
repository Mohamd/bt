@extends('layouts.main')


@section('content')

        <div class="m-4 p-4">
            <div class="container">
                <div class="products_header d-flex justify-content-center align-items-center">
                    <h2 class="text-center">{{trans('site.products')}}</h2>
                </div>
                @forelse ($products as $product)
                <div class="product_body col-lg-4  col-md-6 col-sm-12 col-xs-12 ">
                    <!-- product card -->


                    <div class="row">

                            <div class="thumbnail col-sm-8">
                                <img  alt="{{$product->name}}"
                                     src="{{Storage::url($product->image_kw)}}"

                                     title=""/>

                                <div class="caption text-center">
                                    <h3>{{$product->translate()->name}} </h3>
                                    <h4><p>{{number_format($product->price,2)}} {{trans('site.k.d')}}</p> </h4>
                                    <p class="text-center">
                                        <button  class="btn btn-sm btn-success inc" data-id="{{$product->id}}">
                                            <i class="fa fa-plus"></i>
                                        </button>

                                        <input  id="product_{{$product->id}}" type="text" class="product_number text-center" placeholder="1" value="1"/>

                                        <button class="btn btn-sm btn-warning dec" data-id="{{$product->id}}">
                                            <i class="fa fa-minus"></i>
                                        </button>


                                    </p>
                                    <p class="text-center"><a href="#" v-on:click.prevent="add_to_cart({{$product->id}},true)"  class="btn btn-primary btn-sm buy_button" data-id="{{$product->id}}" role="button">{{trans('site.buy_now')}}</a>
                                        <a href="#" v-on:click.prevent="add_to_cart({{$product->id}})" class="btn btn-default btn-sm " role="button">{{trans('site.add_to_cart')}}</a></p>
                                </div>
                            </div>

                    </div>



                    <!-- end product cart -->
                </div>
                @empty

                    <h2 class="text-center">{{trans('site.no_products')}}</h2>

                @endforelse

            </div>

        </div>

@endsection
