@extends('layouts.main')

@section('content')
    <div id="content" class="col-sm-12 item-article">
        <div class="so-page-builder">
            <div class="container page-builder-ltr">
                <div class="row row_e8kb  row-style ">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_8552  col-style">

                        <div class="module sohomepage-slider ">
                            <h3 class="modtitle" style="padding:20px 0;">{{trans('site.before_and_after')}}</h3>



                            <div class="modcontent">

                                <div id="sohomepage-slider1">
                                    <div class="popup-gallery" style="min-height: 80vh">

                                        @forelse ($images as $image)
                                            <a href="{{\Illuminate\Support\Facades\Storage::url($image->image)}}" title="Before And After"><img src="{{\Illuminate\Support\Facades\Storage::url($image->image)}}" width="200" height="200"></a>

                                            @empty

                                        @endforelse
                                    </div>
                                </div>
                            </div> <!--/.modcontent-->



                        </div>


                    </div>

                </div>
            </div>

        </div>

        <p><br></p>
    </div>
@endsection

@push('css')
    <link href="{{asset('css/magnific-popup.css')}}" rel="stylesheet">
@endpush

@push('js')
    <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>

    <script defer>
        $(document).ready(function() {
            $('.popup-gallery').magnificPopup({
                delegate: 'a',
                type: 'image',
                // tLoading: 'Loading image #%curr%...',
                mainClass: 'mfp-img-mobile',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                },

            });
        });
    </script>
@endpush
