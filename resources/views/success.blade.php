@extends('layouts.main')

@section('content')
    <div class="row" style="padding-bottom: 20%;min-height: 80vh" >
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h2>{{trans('site.order_success')}}</h2>
                </div>

                <div class="card-body">
                    {!!  trans('site.order_success_message',['order_id'=>session()->get('order_id')])!!}
                </div>
            </div>
        </div>

    </div>


@endsection
