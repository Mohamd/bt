@extends('voyager::bread.browse')


@section('page_header')
    @parent

    <div class="row">
        <div class="container">
            <div class="col-md-6">
                <form id="shipping_form" method="post">
                    @csrf
                <label>قم بإختيار وقت زيارة المندوب :</label>
                <input type="text" required id="datetimepicker" form="" value="" class="form-control">

                <input class="btn btn-primary" id="order_shipping_submit" type="submit" value="إنشاء طلب شحن">
                </form>
            </div>


        </div>
        <div class="row">
            <div class="container">
                <div class="col-md-6">
                    <form method="post" action="{{route('print_invoices')}}" id="print_shipping" >
                        @csrf
                        <input type="hidden" id="ids" value="" name="ids" />

                        <button type="submit" class="btn btn-sm btn-success" id="print_invoices">
                            طباعة الفاتورة / الفواتير
                        </button>
                    </form>
                </div>
            </div>
        </div>

    </div>


@endsection

@push('javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.0/axios.min.js" integrity="sha512-DZqqY3PiOvTP9HkjIWgjO6ouCbq+dxqWoJZ/Q+zPYNHmlnI2dQnbJ5bxAHpAMw+LXRm4D72EIRXzvcHQtE8/VQ==" crossorigin="anonymous"></script>
@endpush
