<div class="checkout-content checkout-cart">
    <h2 class="secondary-title"><i class="fa fa-shopping-cart"></i>{{trans('site.shopping_cart')}}  </h2>
    <div class="box-inner">
        <div class="checkout-product">

            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th class="text-left name" colspan="4">{{trans('validation.attributes.name')}}</th>
                    <th class="text-center quantity" colspan="4">{{trans('site.quantity')}}</th>
                    <th class="text-center total" style="color:black;" colspan="4">{{trans('site.price')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($items as $item)
                    <tr>



                        <td class="text-left name" colspan="4">
                            <a
                                href=""><img
                                    src="{{Storage::url($item->associatedModel->image)}}"
                                    width="80"
                                    height="80"
                                    alt="{{$item->associatedModel->name}}" title="{{$item->associatedModel->name}}" class="img-thumbnail"></a>
                            <a href="#"
                               class="product-name">{{$item->associatedModel->translate()->name}}</a>
                        </td>
                        <td class="text-left quantity" colspan="4">
                            <div class="input-group">
                                <input  id="item_{{$item->id}}" data-id="{{$item->id}}"  type="text" name="{{$item->id}}" value="{{$item->quantity}}" size="1"
                                       class="form-control refresh_button">

                            </div>


                        </td>
                        <td class="text-right total" data-id="{{$item->id}}" style="color:black;" colspan="4">{{number_format($item->price *$item->quantity,2)}}

                            {{trans('site.k.d')}}
                            <span data-toggle="tooltip" title="" data-product-key="79" class="btn-delete"
                                  data-original-title="حذف"><i class="fa fa-trash-o"></i></span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                @foreach ($conditions as $condition)
                    <tr>
                        <td colspan="4" class="text-left">{{$condition->getName()}}</td>
                        <td colspan="4" class="text-left"></td>

                        @if ($condition->getType() == 'Coupon')
                            <td class="text-right"> {{$condition->getValue()}} </td>
                            @else
                            <td class="text-right"> {{number_format($condition->getValue(),2)}} {{trans('site.k.d')}}</td>
                        @endif

                    </tr>

                @endforeach


                <tr>
                    <td colspan="4" class="text-left">{{trans('site.total')}}</td>
                    <td colspan="4" class="text-left"></td>
                    <td class="text-right"> {{number_format($total,2)}} {{trans('site.k.d')}} </td>
                </tr>
                </tfoot>
            </table>
        </div>

    </div>
</div>


