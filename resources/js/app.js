/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    methods:{
        add_to_cart:function (product_id,with_redirect=false) {
            $('#product_'+product_id).removeClass('error');
            if ($('#product_'+product_id).val() <= 0) {
                return $('#product_'+product_id).addClass('error');
            }
            axios.post(route('cart.store'),{
                product_id,
                qty: $('#product_'+product_id).val(),
            }).then(function (res) {
                if (with_redirect) {
                    return window.location.href = route('show.checkout');
                }
                swal(res.data);

            }).catch(err=>{

            });
        },

    }
});



// refresh total
function refreshTotal() {
    $('.refresh_button').blur(function () {

        let val = $(this).val();
        let id = $(this).attr('data-id');



        axios.post(route('update.checkout'),{
            item_id:id,
            qty:val,
        }).then(res=>{

            $('#totals').html(res.data)
            refreshTotal();
        })




    })

}



refreshTotal();

// end refresh total

$('#button-coupon').click(function () {
    $('#input-coupon').removeClass('error');
    let coupon = $('#input-coupon').val();
    if (coupon.length <= 0) {
        $('#input-coupon').addClass('error');
    }

    axios.post(route('coupon'),{
        coupon
    }).then(res=>{
         $('#totals').html(res.data.html);
        refreshTotal();
        swal({'text':res.data.msg, icon: 'success',showCancelButton: true});

    }).catch(err=>{
        swal({'text':err.response.data.msg, icon: 'warning',showCancelButton: true});
    })

})



// select Country And Change Dial Code
$(document).ready(function () {

    //zone change
    $('#zone').change(function () {
        let country_code = $(this).val();
        $('.checkout-payment-form #gif_loader').css('display','flex');
        axios.get(route('cities',{
            country_code
        })).then(res => {
            if (res.data.cities.length > 1) {
                let options = '<option disabled selected></option>';
                for (let i = 0; i < res.data.cities.length; i++) {
                     options += `<option>${res.data.cities[i]}</option>`
                }

                $('#city').html(options);
                $('#dial_code').val(res.data.dial_code);
            }

            $('.checkout-payment-form #gif_loader').css('display','none');
        }).catch(e => {
            $('.checkout-payment-form #gif_loader').css('display','none');
        })
    })

    //city change
    $('#city').change(function () {

        $('#payment #gif_loader').css('display','flex');
        let city = $(this).val();

        axios.post(route('calcShipping'),{
            values : {
                city,
            },
            shipping_method:'myfatoorah'
        }).then(res => {
            $('#totals').html(res.data.html);
            refreshTotal();
            $('#payment #gif_loader').css('display','none');
        }).catch(e => {
            $('#payment #gif_loader').css('display','none');
        })
    });


});



