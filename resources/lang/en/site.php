<?php

return [
    "copyright" => "All Copyrights Reversed To :name Ⓒ :year",
    "products"=>"Products",
    "buy_now"=>"Buy Now",
    "add_to_cart"=>"Add To Cart",
    "no_products"=>"There is no products",
    "k.d"=>"K.D",
    "added" => "Products Added To Cart Successfully",
    'area'=>'Area',
    'block'=>'Block',
    'house'=>'House',
    'street'=>'Street',
    'shipping_information'=>'Shipping Information',
    "shipping_cost"=>"Shipping Cost",
    "total"=>"Total",
    'payment_method' => "Payment Method",
    'CASH_ON_DELIVERY'=>'CASH ON DELIVERY',
    'knet_visa'=>'K-net / Visa / Master',
    'coupon_not_valid'=> 'Coupon Is Not Valid Or Expired',
    'coupon_added_successfully'=>'Coupon Added Successfully',
    'coupon'=>'Coupon',
    'apply_coupon'=>'Apply Coupon',
    'loading'=>'Loading . . .',
    'submit_order'=>'Submit Order',
    'order_success'=>'Order Placed Successfully !',
    'order_success_message' => 'Congratulations! Your request has been successfully added. '.
        "\n"
        . "Your order number is :order_id"."\n".
        "Note: Orders for delivery from inside Kuwait take 2-4 working days \n
        As for requests from outside Kuwait, it takes 4 - 9 working days \n
        Thank You ..",

    'error'=>'Error',
    'payment_error'=>"Please Fill All Inputs With Right Information , If the problem recurs, please inform the site administrator thank you",
    'empty_cart'=>'Your Cart Is Empty',
    'zone'=>'Country',
    'postcode'=>'Post Code',
    'home_page'=>"Home Page",
    'from_country'=>'ORDERS IN KUWAIT',
    'out_country'=>'INTERNATIONAL ORDERS',
    'changed' =>'Changed Successfully',
    'shopping_cart'=>'Shopping Cart',
    'quantity'=>'Quantity',
    'price'=>'Price',
    'before_and_after'=>"Before And After",
    'personal_information'=>"Personal Information",
    'notes'=>'Notes',
];
