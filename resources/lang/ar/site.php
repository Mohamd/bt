<?php

return [
    "copyright" => " كل الحقوق محفوظة إلي :name  Ⓒ :year",
    "products"=>"المنتجات",
    "buy_now"=>"الشراء الأن",
    "add_to_cart"=>"اضافة إلي الحقيبة",
    "no_products"=>"لا يوجد منتجات",
    "k.d"=>"د.ك",
    "added" => "تم إضافة المنتج للحقيبة بنجاح",
    'area'=>'منطقة',
    'block'=>'قطعة',
    'house'=>'منزل',
    'street'=>'شارع',
    'shipping_information'=>'معلومات الشحن',
    "shipping_cost"=>"تكلفة الشحن",
    'total'=>'المجموع',
    'payment_method' => 'وسيلة الدفع',
    'CASH_ON_DELIVERY'=> 'الدفع عند الإستلام',
    'knet_visa'=>'كي نت / فيزا / ماستر',
    'coupon_not_valid'=> 'الكوبون قد يكون غير صحيح أو إنتهت مدة صلاحيته',
    'coupon_added_successfully'=>'تم إضافة الكود بنجاح شكراً لك',
    'coupon'=>'كود الخصم',
    'apply_coupon'=>'تطبيق الخصم',
    'loading'=>'جاري التحميل . . .',
    'name'=>'الأسم',
    'submit_order'=>'إتمام الطلب',
    'order_success'=>'تمت إضافة الطلب بنجاح !',
    'order_success_message'=>

        "<div class=''>
            رقم الطلب الخاص بك هو  # <b>:order_id</b>
        </div>
        <div class=''>
        <p>     ملحوظة : تستغرق طلبات التوصيل من داخل الكويت من 2 - 4 أيام عمل</p>
           <p>        اما الطلبات من خارج الكويت فتستغرق 4 - 9 أيام عمل
        شكراً
</p>
        </div>",

    'error'=>'خطأ .',
    'payment_error'=>"برجاء التأكد من ملئ جميع البيانات بصورة صحيحه وإذا تكررت المشكلة برجاء إبلاغ مدير الموقع شكراً",
    'empty_cart'=>'الحقيبة الخاصة بك فارغه !',
    'zone'=>'الدولة',
    'postcode'=>'الرمز البريدي',
    'home_page'=>"الصفحة الرئيسية",
    'from_country'=>'الطلب من داخل الكويت',
    'out_country'=>'الطلب من خارج الكويت',
    'changed' =>'تم التغيير بنجاح',
    'shopping_cart'=>'حقيبة التسوق',
    'quantity'=>'الكمية',
    'price'=>'السعر',
    'before_and_after'=>"قبل و بعد",
    'personal_information'=>"معلوماتك الشخصية",
    'notes'=>'ملاحظات',
];
