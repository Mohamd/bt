<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	public function up()
	{
		Schema::create('orders', function(Blueprint $table) {
			$table->increments('id');
			$table->string('invoice_number', 191)->nullable();
			$table->string('tracking_code')->nullable();
			$table->string('name', 191)->index();
			$table->string('phone', 191)->index();
			$table->string('email', 191)->index();
			$table->string('zone', 191)->nullable();
			$table->string('city', 191)->nullable();
			$table->string('postcode', 191)->nullable();
			$table->text('address');
			$table->text('notes')->nullable();
			$table->float('sub_total', 8,2);
			$table->string('copoun', 191)->nullable();
			$table->float('copoun_price', 8,2)->nullable();
			$table->float('shipping', 8,2)->index();
			$table->integer('total')->index();
			$table->string('payment_method', 191)->nullable();
			$table->string('shipping_method', 191)->nullable();
			$table->integer('order_status_id')->index();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('orders');
	}
}