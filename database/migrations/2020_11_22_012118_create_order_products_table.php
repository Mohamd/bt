<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderProductsTable extends Migration {

	public function up()
	{
		Schema::create('order_products', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('order_id')->index();
			$table->string('product_id')->index();
			$table->float('total', 8,2)->index();
			$table->integer('quantity')->index();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('order_products');
	}
}