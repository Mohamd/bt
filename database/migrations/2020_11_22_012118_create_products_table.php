<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	public function up()
	{
		Schema::create('products', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 191)->index();
			$table->float('price', 8,2);
			$table->float('new_price', 8,2)->index();
			$table->string('image', 191);
			$table->string('image_kw', 191)->index();
			$table->integer('stock')->unsigned()->index();
			$table->float('width', 8,2);
			$table->float('height', 8,2)->index();
			$table->float('length', 8,2)->index();
			$table->float('depth', 8,2)->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('products');
	}
}